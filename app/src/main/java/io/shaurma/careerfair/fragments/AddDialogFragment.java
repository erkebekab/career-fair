package io.shaurma.careerfair.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import java.text.ParseException;
import java.util.Calendar;

import io.shaurma.careerfair.DetailActivity;
import io.shaurma.careerfair.MainActivity;
import io.shaurma.careerfair.R;
import io.shaurma.careerfair.helpers.CustomDate;
import io.shaurma.careerfair.models.FairItem;

public class AddDialogFragment extends DialogFragment {
    private static final String TAG = "addDialogTag";
    RecyclerView.ViewHolder viewHolder;
    RecyclerView.Adapter adapter;

    FairItem selectedItem;
    TimePicker timePicker;
    TextView timeOverlap;
    ToggleButton importantBtn;
    boolean editDialog;

    private CustomDate beginDate;
    private CustomDate endDate;
    private int duration;
    private int hour;
    private int minute;

    public void setSelectedItem(FairItem selectedItem) {
        this.selectedItem = selectedItem;
    }

    public FairItem getSelectedItem() {
        return selectedItem;
    }

    public void setViewHolder(RecyclerView.ViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }

    public boolean isEditDialog() {
        return editDialog;
    }

    public void setEditDialog(boolean editDialog) {
        this.editDialog = editDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (isEditDialog()) {
            return loadEditDialog();
        }
        return loadAddDialog();
    }

    private Dialog loadAddDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.modal_add, null);

        final Calendar myCalender = Calendar.getInstance();
        hour = myCalender.get(Calendar.HOUR_OF_DAY);
        minute = myCalender.get(Calendar.MINUTE);

        loadTimePicker(view);
        if (getSelectedItem().hasTimeSpecified()) {
            timePicker.setEnabled(false);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(getSelectedItem().getBeginDate());
            hour = calendar.get(Calendar.HOUR_OF_DAY);
            minute = calendar.get(Calendar.MINUTE);
        }
        loadToggleButton(view);

        builder.setView(view);
        builder.setTitle("Add exhibitor")
                .setIcon(R.drawable.ic_sharp_add_black_24px)
                .setPositiveButton("Add", (dialog, which) -> {
                    getSelectedItem().planToTime(beginDate.getDate(), endDate.getDate(), duration);
                    getSelectedItem().setImportant(importantBtn.isChecked());
                    MainActivity.getUserList().add(getSelectedItem());
                    Log.d(TAG, "added");
                    notifyParent();
                });

        builder.setNegativeButton("Cancel", (dialog, which) -> {});
        return builder.create();
    }

    private Dialog loadEditDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        Log.d(TAG, getSelectedItem().getBegin());
        Log.d(TAG, getSelectedItem().getBeginDate().toString());
        Log.d(TAG, String.valueOf(getSelectedItem().isImportant()));

        View view = inflater.inflate(R.layout.modal_add, null);

        final Calendar myCalender = Calendar.getInstance();
        myCalender.setTime(getSelectedItem().getBeginDate());
        hour = myCalender.get(Calendar.HOUR_OF_DAY);
        minute = myCalender.get(Calendar.MINUTE);

        loadTimePicker(view);
        loadToggleButton(view);

        builder.setView(view);
        builder.setTitle("Edit exhibitor")
                .setIcon(R.drawable.ic_pencil_edit_button_black_24)
                .setPositiveButton("Save", (dialog, which) -> {
                    if (getSelectedItem().getBeginDate() != null) {
                        getSelectedItem().planToTime(beginDate.getDate(), endDate.getDate(), duration);
                        getSelectedItem().setImportant(importantBtn.isChecked());
                        Log.d(TAG, "saved");
                    } else {
                        Log.d(TAG, "not saved");
                    }
                    notifyParent();
                });

        builder.setNegativeButton("Cancel", (dialog, which) -> {});

        builder.setNeutralButton("Delete", (dialog, which) -> {
            MainActivity.getUserList().remove(getSelectedItem());
            notifyParent();
        });
        return builder.create();
    }

    public void notifyParent() {
        if (getAdapter() != null) {
            getAdapter().notifyDataSetChanged();
        } else {
            ((DetailActivity)getActivity()).loadButtons();
        }
    }

    private void loadTimePicker(View view) {
        timeOverlap = view.findViewById(R.id.tv_time_overlap);

        timePicker = view.findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);

        timePicker.setOnTimeChangedListener((view1, hourOfDay, minute12)
                -> checkForCollide(timePicker, timeOverlap, hourOfDay, minute12));
    }

    private void checkForCollide(TimePicker timePicker, TextView timeOverlap, int hourOfDay, int minute12) {
        try {
            beginDate = new CustomDate(hourOfDay + ":" + minute12, "HH:mm");
            endDate = new CustomDate(hourOfDay + ":" + minute12, "HH:mm");
            if (getSelectedItem().getTimeRequiredToVisit() != null) {
                CustomDate minutesDate = new CustomDate(getSelectedItem().getTimeRequiredToVisit(), "HH:mm:ss");
                duration = minutesDate.getCalendar().get(Calendar.MINUTE) + minutesDate.getCalendar().get(Calendar.HOUR_OF_DAY)*60;
            }
            endDate.addMinutes(duration);

            if (!isDateCollidingWithDatesInUserList(beginDate, endDate)) {
                Log.d(TAG, "not colliding");
                timePicker.setBackgroundColor(getResources().getColor(R.color.colorTimeValid));
                timeOverlap.setTextColor(getResources().getColor(android.R.color.transparent));
                ((AlertDialog)getDialog()).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
            } else {
                Log.d(TAG, "colliding");
                timeOverlap.setTextColor(getResources().getColor(R.color.colorTimeNotValid));
                timePicker.setBackgroundColor(getResources().getColor(R.color.colorTimeNotValid));
                ((AlertDialog)getDialog()).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void loadToggleButton(View view) {
        importantBtn = view.findViewById(R.id.btn_star_toggle);
        importantBtn.setChecked(getSelectedItem().isImportant());
        setToggleBtnColor();
        importantBtn.setOnClickListener(v -> {
            setToggleBtnColor();
            getSelectedItem().setImportant(importantBtn.isChecked());
        });
    }

    private void setToggleBtnColor() {
        importantBtn.setButtonDrawable(importantBtn.isChecked() ?
                R.drawable.ic_star_fill : R.drawable.ic_star_no_fill);
        importantBtn.setTextColor(importantBtn.isChecked() ?
                Color.RED : Color.BLACK);
    }

    private boolean isDateCollidingWithDatesInUserList(CustomDate beginDate, CustomDate endDate) {
        for (FairItem item : MainActivity.getUserList()) {
            if(item.equals(getSelectedItem())) continue;
            if (CustomDate.ifDatesColliding(item.getBeginDate(), item.getEndDate(),
                    beginDate.getDate(), endDate.getDate())) {
                return true;
            }
        }
        return false;
    }

    public TimePicker getTimePicker() {
        return timePicker;
    }

    @Override
    public void onStart() {
        super.onStart();
        timePicker.setCurrentHour(hour);
        timePicker.setCurrentMinute(minute);
        checkForCollide(timePicker, timeOverlap, hour, minute);
    }

    public RecyclerView.Adapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
    }
}
