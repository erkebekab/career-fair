package io.shaurma.careerfair.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;

import io.shaurma.careerfair.MainActivity;
import io.shaurma.careerfair.R;
import io.shaurma.careerfair.adapters.EventsRVAdapter;
import io.shaurma.careerfair.adapters.ExhibitorsRVAdapter;
import io.shaurma.careerfair.helpers.CacheHelper;
import io.shaurma.careerfair.helpers.JsonHelper;
import io.shaurma.careerfair.helpers.VolleyCallback;
import io.shaurma.careerfair.models.Event;
import io.shaurma.careerfair.models.Exhibitor;
import io.shaurma.careerfair.models.FairItem;

public class EventFragment extends Fragment {

    RecyclerView rv;
    SwipeRefreshLayout mSwipeRefreshLayout;

    private String TAG = "LECTURE_FRAGMENT";

     static  List<Event> events = new ArrayList<>();

    boolean isInit = false;

    private final String EVENTS_URL = "http://lab.wikway.de/companies/zwik-additional-events";
    private final String EVENTS_FILE_NAME = "events";


    public EventFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event, container, false);

        rv = (RecyclerView) view.findViewById(R.id.rv_event);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container_event);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                parseEvents();
            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        //loadEvents();
        new MyTask().execute();

        EventsRVAdapter adapter = new EventsRVAdapter(events, (MainActivity) getActivity());
        rv.setAdapter(adapter);


        return view;
    }

    private Fragment loadEvents() {
        String json = CacheHelper.retrieve(getActivity(), EVENTS_FILE_NAME);
        if (json.length() > 0) {
            events = new JsonHelper<Event>().getArrayFromJson(json, Event.class);
            isInit = true;
        } else {
            parseEvents();
        }
        if (isInit) {
            for (FairItem item : events) {
                item.loadDates();
            }
            mSwipeRefreshLayout.setRefreshing(false);

            return this;
        }

        return this;
    }

    public void parseEvents() {
        mSwipeRefreshLayout.setRefreshing(true);
        VolleyCallback exhibitorCallback = (json) -> {
            if (this.isVisible()) {
                events = new JsonHelper<Event>().getArrayFromJson(json, Event.class);
                isInit = true;
                CacheHelper.save(getActivity(), EVENTS_FILE_NAME, json);
                loadEvents();
                if(this.isVisible()) {
                    EventsRVAdapter adapter = new EventsRVAdapter(events, (MainActivity) getActivity());
                    rv.setAdapter(adapter);
                }
            }
        };
        makeGetRequest(EVENTS_URL, exhibitorCallback);
    }

    public void makeGetRequest(String url, final VolleyCallback callback) {
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> callback.onSuccess(response),
                error -> System.out.println("Error"));
        queue.add(stringRequest);
    }

    public static List<Event> getEvents() {
        return events;
    }

    class MyTask extends AsyncTask<Void, Void, Fragment> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mSwipeRefreshLayout.setRefreshing(true);
        }

        @Override
        protected Fragment doInBackground(Void... params) {
            return loadEvents();
        }

        @Override
        protected void onPostExecute(Fragment result) {
            super.onPostExecute(result);

            if(result.isVisible()) {
                EventsRVAdapter adapter = new EventsRVAdapter(events, (MainActivity) getActivity());
                rv.setAdapter(adapter);
            }
        }
    }

}
