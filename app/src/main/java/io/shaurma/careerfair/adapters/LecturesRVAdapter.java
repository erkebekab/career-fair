package io.shaurma.careerfair.adapters;

import android.app.FragmentManager;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import io.shaurma.careerfair.DetailActivity;
import io.shaurma.careerfair.MainActivity;
import io.shaurma.careerfair.R;
import io.shaurma.careerfair.fragments.AddDialogFragment;
import io.shaurma.careerfair.models.Lecture;

public class LecturesRVAdapter extends RecyclerView.Adapter<LecturesRVAdapter.LecturesViewHolder>  {

    private static final String TAG = "lecturesAdapter";

    public static class LecturesViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView lectrureTitle;
        TextView time;
        TextView companyName;
        TextView timeReq;
        public FloatingActionButton floatingActBtn;


        LecturesViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv_lectures);
            lectrureTitle = (TextView)itemView.findViewById(R.id.home_name);
            time = (TextView)itemView.findViewById(R.id.lecture_time);
            companyName = (TextView)itemView.findViewById(R.id.home_name2);
            timeReq = (TextView)itemView.findViewById(R.id.lecture_time_req);
            floatingActBtn = itemView.findViewById(R.id.lecture_fab);
        }
    }

    List<Lecture> lectures;
    MainActivity mainActivity;

    public LecturesRVAdapter(List<Lecture> lectures, MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.lectures = lectures;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public LecturesRVAdapter.LecturesViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lecture_item, viewGroup, false);
        LecturesRVAdapter.LecturesViewHolder pvh = new LecturesRVAdapter.LecturesViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(LecturesRVAdapter.LecturesViewHolder viewHolder, int i) {
        viewHolder.cv.setOnClickListener(v -> {
            Intent intent = new Intent(getMainActivity(), DetailActivity.class);
            intent.putExtra(DetailActivity.fairItemToSendId, i);
            intent.putExtra(DetailActivity.fairItemToSendType, DetailActivity.fairItemToSendTypeLecture);
            getMainActivity().startActivity(intent);
        });
        String timeBegin = lectures.get(i).getBegin() + " - " + lectures.get(i).getEnd();
        String company = "by "+ lectures.get(i).getCompanyName();

        viewHolder.companyName.setText(company);
        viewHolder.lectrureTitle.setText(lectures.get(i).getLectureTitle());
        viewHolder.time.setText(timeBegin);
        viewHolder.timeReq.setText(lectures.get(i).getDuration());

        if (MainActivity.getUserList().contains(lectures.get(i))) {
            loadRemoveButton(viewHolder, i);
        } else {
            loadAddButton(viewHolder, i);
        }
    }

    private void loadAddButton(LecturesViewHolder viewHolder, int position) {
        viewHolder.floatingActBtn.setBackgroundTintList(viewHolder.itemView.getResources().getColorStateList(android.R.color.holo_green_light));
        viewHolder.floatingActBtn.setImageResource(R.drawable.ic_sharp_add_white_24px);
        viewHolder.floatingActBtn.setOnClickListener(v -> {
            Log.d(TAG, "Add Button clicked. id: " + lectures.get(position).getCompanyName());
            FragmentManager manager = getMainActivity().getFragmentManager();
            AddDialogFragment dialogFragment = new AddDialogFragment();
            dialogFragment.setAdapter(this);
            dialogFragment.setViewHolder(viewHolder);
            dialogFragment.setSelectedItem(lectures.get(position));
            dialogFragment.show(manager, "dialog");
        });
    }

    private void loadRemoveButton(LecturesViewHolder viewHolder, int position) {
        viewHolder.floatingActBtn.setBackgroundTintList(viewHolder.itemView.getResources().getColorStateList(android.R.color.holo_red_light));
        viewHolder.floatingActBtn.setImageResource(R.drawable.ic_pencil_edit_button_white_24);
        viewHolder.floatingActBtn.setOnClickListener(v -> {
            Log.d(TAG, "Edit Button clicked. id: " + lectures.get(position).getCompanyName());
            FragmentManager manager = getMainActivity().getFragmentManager();
            AddDialogFragment dialogFragment = new AddDialogFragment();
            dialogFragment.setViewHolder(viewHolder);
            dialogFragment.setAdapter(this);
            dialogFragment.setSelectedItem(MainActivity.getFromUserList(lectures.get(position)));
            dialogFragment.setEditDialog(true);
            dialogFragment.show(manager, "dialog");
        });
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    @Override
    public int getItemCount() {
        return lectures.size();
    }
}
