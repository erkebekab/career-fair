package io.shaurma.careerfair.adapters;

import android.app.FragmentManager;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import io.shaurma.careerfair.DetailActivity;
import io.shaurma.careerfair.MainActivity;
import io.shaurma.careerfair.R;
import io.shaurma.careerfair.fragments.AddDialogFragment;
import io.shaurma.careerfair.models.Event;

public class EventsRVAdapter extends RecyclerView.Adapter<EventsRVAdapter.EventsViewHolder> {

    private static final String TAG = "eventsAdapter";

    public static class EventsViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView time;
        TextView timeReq;
        TextView companyName;
        CardView cv;

        public FloatingActionButton floatingActBtn;


        EventsViewHolder(View itemView) {
            super(itemView);
            companyName = itemView.findViewById(R.id.home_name2);
            cv = itemView.findViewById(R.id.cv_lectures);
            title = (TextView) itemView.findViewById(R.id.home_name);
            time = (TextView) itemView.findViewById(R.id.lecture_time);
            timeReq = (TextView) itemView.findViewById(R.id.lecture_time_req);
            floatingActBtn = itemView.findViewById(R.id.lecture_fab);
        }
    }

    List<Event> events;
    MainActivity mainActivity;

    public EventsRVAdapter(List<Event> events, MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.events = events;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public EventsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lecture_item, viewGroup, false);
        EventsViewHolder pvh = new EventsViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(EventsViewHolder viewHolder, int i) {
        viewHolder.cv.setOnClickListener(v -> {
            Intent intent = new Intent(getMainActivity(), DetailActivity.class);
            intent.putExtra(DetailActivity.fairItemToSendId, i);
            intent.putExtra(DetailActivity.fairItemToSendType, DetailActivity.fairItemToSendTypeEvent);
            getMainActivity().startActivity(intent);
        });
        String timeBegin = events.get(i).getBegin() + " - " + events.get(i).getEnd();

        viewHolder.title.setText(events.get(i).getName());
        viewHolder.companyName.setHeight(0);
        viewHolder.time.setText(timeBegin);
        viewHolder.timeReq.setText(events.get(i).getDuration());

        if (MainActivity.getUserList().contains(events.get(i))) {
            loadRemoveButton(viewHolder, i);
        } else {
            loadAddButton(viewHolder, i);
        }
    }

    private void loadAddButton(EventsViewHolder viewHolder, int position) {
        viewHolder.floatingActBtn.setBackgroundTintList(viewHolder.itemView.getResources().getColorStateList(android.R.color.holo_green_light));
        viewHolder.floatingActBtn.setImageResource(R.drawable.ic_sharp_add_white_24px);
        viewHolder.floatingActBtn.setOnClickListener(v -> {
            Log.d(TAG, "Add Button clicked. id: " + events.get(position).getName());
            FragmentManager manager = getMainActivity().getFragmentManager();
            AddDialogFragment dialogFragment = new AddDialogFragment();
            dialogFragment.setAdapter(this);
            dialogFragment.setViewHolder(viewHolder);
            dialogFragment.setSelectedItem(events.get(position));
            dialogFragment.show(manager, "dialog");
        });
    }

    private void loadRemoveButton(EventsViewHolder viewHolder, int position) {
        viewHolder.floatingActBtn.setBackgroundTintList(viewHolder.itemView.getResources().getColorStateList(android.R.color.holo_red_light));
        viewHolder.floatingActBtn.setImageResource(R.drawable.ic_pencil_edit_button_white_24);
        viewHolder.floatingActBtn.setOnClickListener(v -> {
            Log.d(TAG, "Edit Button clicked. id: " + events.get(position).getName());
            FragmentManager manager = getMainActivity().getFragmentManager();
            AddDialogFragment dialogFragment = new AddDialogFragment();
            dialogFragment.setViewHolder(viewHolder);
            dialogFragment.setAdapter(this);
            dialogFragment.setSelectedItem(MainActivity.getFromUserList(events.get(position)));
            dialogFragment.setEditDialog(true);
            dialogFragment.show(manager, "dialog");
        });
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    @Override
    public int getItemCount() {
        return events.size();
    }
}
