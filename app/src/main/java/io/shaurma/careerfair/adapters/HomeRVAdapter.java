package io.shaurma.careerfair.adapters;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import io.shaurma.careerfair.DetailActivity;
import io.shaurma.careerfair.MainActivity;
import io.shaurma.careerfair.R;
import io.shaurma.careerfair.fragments.AddDialogFragment;
import io.shaurma.careerfair.models.Event;
import io.shaurma.careerfair.models.Exhibitor;
import io.shaurma.careerfair.models.FairItem;
import io.shaurma.careerfair.models.Lecture;

public class HomeRVAdapter extends RecyclerView.Adapter<HomeRVAdapter.HomeViewHolder> {

    private static final String TAG = "homeAdapter";

    public static class HomeViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        ConstraintLayout layout;
        TextView name;
        TextView name2;
        ImageView logo;
        TextView time;
        TextView duration;
        public FloatingActionButton floatingActBtn;

        HomeViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv_home);
            layout = itemView.findViewById(R.id.cl_item_layout);
            name = itemView.findViewById(R.id.home_name);
            name2 = (TextView) itemView.findViewById(R.id.home_name2);
            logo = (ImageView) itemView.findViewById(R.id.home_logo);
            time = (TextView) itemView.findViewById(R.id.home_time);
            duration = (TextView) itemView.findViewById(R.id.home_req);
            floatingActBtn = (FloatingActionButton) itemView.findViewById(R.id.home_fab);
        }
    }

    List<FairItem> fairItems;
    Activity mainActivity;


    public HomeRVAdapter(List<FairItem> fairItems, MainActivity mainActivity) {
        this.fairItems = fairItems;
        this.mainActivity = mainActivity;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public HomeRVAdapter.HomeViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.home_item, viewGroup, false);
        HomeRVAdapter.HomeViewHolder pvh = new HomeRVAdapter.HomeViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(HomeRVAdapter.HomeViewHolder homeViewHolder, int i) {
        if (fairItems.get(i) instanceof Exhibitor) {
            Exhibitor exhibitor = (Exhibitor) fairItems.get(i);

            String time = exhibitor.getBegin() + " - " + exhibitor.getEnd();
            homeViewHolder.name.setText(exhibitor.getName());
            homeViewHolder.name2.setHeight(0);
            homeViewHolder.duration.setText(exhibitor.getDuration());
            homeViewHolder.time.setText(time);

            Picasso.get().load(exhibitor.getLogo())
                    .resize(250, 250)
                    .centerInside().into(homeViewHolder.logo);
        }

        else if (fairItems.get(i) instanceof Lecture) {
            Lecture lecture = (Lecture) fairItems.get(i);

            String time = lecture.getBegin() + " - " + lecture.getEnd();

            homeViewHolder.name.setText(lecture.getLectureTitle());
            homeViewHolder.name2.setText("by " + lecture.getCompanyName());
            homeViewHolder.duration.setText(lecture.getDuration());
            homeViewHolder.time.setText(time);
        }

        else if (fairItems.get(i) instanceof Event) {
            Event event = (Event) fairItems.get(i);

            String time = event.getBegin() + " - " + event.getEnd();

            homeViewHolder.name.setText(event.getName());
            homeViewHolder.name2.setHeight(0);
            homeViewHolder.duration.setText(event.getDuration());
            homeViewHolder.time.setText(time);
        }

        homeViewHolder.layout.setBackgroundColor(homeViewHolder.
                itemView.getResources().getColor(fairItems.get(i).isImportant()
                    ? R.color.colorImpFairItem : R.color.colorNotImpFairItem));

        homeViewHolder.floatingActBtn.setOnClickListener(v -> {
            Log.d(TAG, "Edit Button clicked. id: " + fairItems.get(i).toString());
            FragmentManager manager = getMainActivity().getFragmentManager();
            AddDialogFragment dialogFragment = new AddDialogFragment();
            dialogFragment.setViewHolder(homeViewHolder);
            dialogFragment.setAdapter(this);
            dialogFragment.setSelectedItem(MainActivity.getFromUserList(fairItems.get(i)));
            dialogFragment.setEditDialog(true);
            dialogFragment.show(manager, "dialog");
            this.notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return fairItems.size();
    }

    public Activity getMainActivity() {
        return mainActivity;
    }

}
