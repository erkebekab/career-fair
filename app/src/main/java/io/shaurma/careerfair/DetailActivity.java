package io.shaurma.careerfair;

import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.shaurma.careerfair.fragments.AddDialogFragment;
import io.shaurma.careerfair.fragments.EventFragment;
import io.shaurma.careerfair.fragments.ExhibitorsFragment;
import io.shaurma.careerfair.fragments.LectureFragment;
import io.shaurma.careerfair.models.Event;
import io.shaurma.careerfair.models.Exhibitor;
import io.shaurma.careerfair.models.FairItem;
import io.shaurma.careerfair.models.Lecture;

public class DetailActivity extends AppCompatActivity {

    private final static String TAG = "detailsActivity";

    ImageView imageView;
    TextView detailsName;
    TextView detailsDescription;
    FloatingActionButton linkBtn;
    FloatingActionButton editBtn;
    CardView cardView;

    FairItem selectedItem;

    public static final String fairItemToSendId = "io.shaurma.careerfair.adapters.ExhibitorsRVAdapter.fairItemToSendId";
    public static final String fairItemToSendType = "io.shaurma.careerfair.adapters.ExhibitorsRVAdapter.fairItemToSendType";
    public static final int fairItemToSendTypeExhibitor = 1;
    public static final int fairItemToSendTypeLecture = 2;
    public static final int fairItemToSendTypeEvent = 3;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();

        int type = bundle.getInt(DetailActivity.fairItemToSendType);
        switch (type) {
            case DetailActivity.fairItemToSendTypeExhibitor:
                selectedItem = ExhibitorsFragment.getExhibitors().get(bundle.getInt(fairItemToSendId));
                break;
            case DetailActivity.fairItemToSendTypeLecture:
                selectedItem = LectureFragment.getLectures().get(bundle.getInt(fairItemToSendId));
                break;
            case DetailActivity.fairItemToSendTypeEvent:
                selectedItem = EventFragment.getEvents().get(bundle.getInt(fairItemToSendId));
                break;
        }

        cardView = findViewById(R.id.details_card);

        imageView = findViewById(R.id.details_logo);
        detailsName = findViewById(R.id.details_name);
        detailsDescription = findViewById(R.id.details_text);
        editBtn = findViewById(R.id.fab_edit);
        linkBtn = findViewById(R.id.fab_link);

        if (selectedItem instanceof Exhibitor) {
            Exhibitor exhibitor = (Exhibitor)selectedItem;
            Picasso.get().load(exhibitor.getLogo())
                    .resize(250, 100)
                    .centerInside().into(imageView);
            detailsName.setText(exhibitor.getName());
            detailsDescription.setText(exhibitor.getDescription());
        } else if (selectedItem instanceof Lecture) {
            Lecture lecture = (Lecture)selectedItem;
            detailsDescription.setText(lecture.getCompanyName());
            detailsName.setText(lecture.getLectureTitle());

        } else if (selectedItem instanceof Event) {
            Event event = (Event)selectedItem;
            detailsName.setText(event.getName());
            detailsDescription.setText(event.getDescription());

        }
        detailsDescription.setMovementMethod(new ScrollingMovementMethod());

        loadButtons();
    }

    public boolean onOptionsItemSelected(MenuItem item){
        finish();

        return true;
    }
    public void loadButtons() {
        if (MainActivity.getUserList().contains(selectedItem)) {
            loadRemoveButton();
        } else {
            loadAddButton();
        }
        loadLinkButton();
    }

    private void loadLinkButton() {
        if (selectedItem instanceof Exhibitor) {
            linkBtn.setOnClickListener(v -> {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(((Exhibitor)selectedItem).getLink()));
                startActivity(i);
            });
        } else {
            linkBtn.hide();
        }
    }


    private void loadAddButton() {
        editBtn.setBackgroundTintList(getResources().getColorStateList(android.R.color.holo_green_light));
        editBtn.setImageResource(R.drawable.ic_sharp_add_white_24px);
        editBtn.setOnClickListener(v -> {
            FragmentManager manager = this.getFragmentManager();
            AddDialogFragment dialogFragment = new AddDialogFragment();
            dialogFragment.setSelectedItem(selectedItem);
            dialogFragment.show(manager, "dialog");
        });
    }

    private void loadRemoveButton() {
        editBtn.setBackgroundTintList(getResources().getColorStateList(android.R.color.holo_red_light));
        editBtn.setImageResource(R.drawable.ic_pencil_edit_button_white_24);
        editBtn.setOnClickListener(v -> {
            FragmentManager manager = this.getFragmentManager();
            AddDialogFragment dialogFragment = new AddDialogFragment();
            dialogFragment.setSelectedItem(MainActivity.getFromUserList(selectedItem));
            dialogFragment.setEditDialog(true);
            dialogFragment.show(manager, "dialog");
        });
    }

}
