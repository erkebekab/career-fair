package io.shaurma.careerfair.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class CustomDate {
    Calendar time;

    public CustomDate(String newTime, String format) throws ParseException {
        Date date = new SimpleDateFormat(format).parse(newTime);
        this.time = GregorianCalendar.getInstance();
        this.time.setTime(date);

    }

    public Date getDate() {
        return time.getTime();
    }

    public void addMinutes(int minutes) {
        time.add(Calendar.MINUTE, minutes);
    }

    public Calendar getCalendar() {
        return time;
    }

    public long getValue() {
        return getDate().getTime();
    }

    public static boolean ifDatesColliding(Date beginA, Date endA, Date beginB, Date endB) {
        return !(endA.getTime() <= beginB.getTime()
                || beginA.getTime() >= endB.getTime());
    }
}
