package io.shaurma.careerfair.helpers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class JsonHelper<T> {

    public List<T> getArrayFromJson(String json, Class<T> myType) {
        Type collectionType = TypeToken.getParameterized(List.class, myType).getType();
        return new Gson().fromJson(json, collectionType);
    }

    public String getStringFromJson(List<T> list, Class<T> myType) {
        Type collectionType = TypeToken.getParameterized(List.class, myType).getType();
        return new Gson().toJson(list, collectionType);
    }
}
