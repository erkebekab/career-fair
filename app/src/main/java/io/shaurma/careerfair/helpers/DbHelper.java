package io.shaurma.careerfair.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "shaurma_io_storage.db";

    public static final String EXHIBITORS_TABLE_NAME = "exhibitors";
    public static final String LECTURES_TABLE_NAME = "lectures";
    public static final String EVENTS_TABLE_NAME = "events";
    public static final String USER_FAIR_ITEMS_TABLE_NAME = "user_fair_items";


    public DbHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + EXHIBITORS_TABLE_NAME +
                " (id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(255) NOT NULL, " +
                "description TEXT, link varchar(255), logo varchar(255), " +
                "time_required_to_visit varchar(255));");

        db.execSQL("CREATE TABLE " + LECTURES_TABLE_NAME +
                " (id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(255) NOT NULL, " +
                "description TEXT, link varchar(255), logo varchar(255), " +
                "time_required_to_visit varchar(255));");

        db.execSQL("CREATE TABLE " + EVENTS_TABLE_NAME +
                " (id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(255) NOT NULL, " +
                "description TEXT, link varchar(255), logo varchar(255), " +
                "time_required_to_visit varchar(255));");

        db.execSQL("CREATE TABLE " + USER_FAIR_ITEMS_TABLE_NAME +
                " (id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(255) NOT NULL, " +
                "description TEXT, link varchar(255), logo varchar(255), " +
                "time_required_to_visit varchar(255));");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
