package io.shaurma.careerfair.helpers;

import android.content.Context;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;

public class CacheHelper {

    public static void save(Context context, String key, String value) {
        try {
            key = URLEncoder.encode(key, "UTF-8");
            key += ".json";

            FileOutputStream fos = context.openFileOutput(key, Context.MODE_PRIVATE);
            fos.write(value.getBytes());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String retrieve(Context context, String key) {
        try {
            key = URLEncoder.encode(key, "UTF-8");
            key += ".json";

            FileInputStream fis = context.openFileInput(key);
            InputStreamReader inputStreamReader = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            fis.close();
            inputStreamReader.close();

            return stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
